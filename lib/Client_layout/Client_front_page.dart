import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Databse/Databse.dart';
import 'package:flutter_app/Client_layout/profile.dart';
import 'package:flutter_app/Client_layout/MainProfile.dart';
import 'package:flutter_app/Client_layout/OrderPage.dart';
import 'package:flutter_app/Client_layout/Notification.dart';
import 'package:flutter_app/Vendor_layout/VendorDashboard.dart';



//void main() {
//  runApp(MaterialApp(
//    home: Client_front_page(),
//  ));
//}

class Client_front_page extends StatefulWidget{
  front_page createState() => front_page();
  bool flag;
  Client_front_page({
    this.flag,
  });
}

class front_page extends State<Client_front_page> with TickerProviderStateMixin{
  TabController controller;
  TabController Vendor_Controller;
  int selectIndex=0;
  final database = Database();
  final Profilepage= myProfile();
  final Orders = OrderState();
  final Notifications = NotifyState();
  final VendorDash = Dashboard();

  @override
  void initState() {
   // database.getClientsNotification();
    controller = TabController(length: 2,vsync: this);
    if(!widget.flag){
      Vendor_Controller = TabController(length: 2,vsync: this);
    }
    database.getOrdersDashboard();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              if(selectIndex == 0)
              CheckPage0(),
              if(selectIndex == 4)
              Checkpage4(),
              if(selectIndex == 2)
              CheckPage2(),
              if(selectIndex == 3)
                Container(
                    height: 550,
                    child: Checkpage3()),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: BottomNavigationBar(items: const<BottomNavigationBarItem>[
                    BottomNavigationBarItem(
                      icon: Icon(Icons.home,color: Colors.blueGrey,size: 30,),
                      title: Text("Home"),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(Icons.message,color: Colors.blueGrey),
                      title: Text("Message"),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(Icons.content_paste,color: Colors.blueGrey),
                      title: Text("Order"),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(Icons.notifications,color: Colors.blueGrey),
                      title: Text("Pings"),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(Icons.account_circle,color: Colors.blueGrey,),
                      title: Text("Account"),
                    ),
                  ],
                  currentIndex: selectIndex,
                  onTap: OnTapItem,
                  selectedItemColor: Colors.black,
                  //showSelectedLabels: false,
                  showUnselectedLabels: false,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void OnTapItem(int index){
    setState(() {
      selectIndex=index;
    });
  }

  Widget Checkpage3(){
    if(widget.flag){
      return Notifications.notify_Client();
    }else{
      return Notifications.notify_Vendor();
    }
  }

  Widget Checkpage4(){
    if(widget.flag){
      return Profilepage.BuilderClient();
    }else{
      return Profilepage.BuilderVendor();
    }
  }

  Widget CheckPage0(){
    if(widget.flag){
      return ClientDashboard();
    }else{
      return VendorDashboard();
    }
  }

  Widget CheckPage2(){
    if(widget.flag){
      return ShowOrdersClients();
    }else{
      return ShowOrdersVendors();
    }
  }

  Widget VendorDashboard(){
    return Container(
      height: 550,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Text("DASHBOARD",style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold)),
            spacing(16),
            VendorDash.OrderPendingCard(),
            spacing(32),
            ShowDownStarts(),
            DashboardInfo()
          ],
        ),
      ),
    );
  }

  Widget DashboardNamePart(String name){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(left: 16),
            child: Text("Hello "+ name,style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold))),
        Container(
            margin: EdgeInsets.only(right: 16),
            child: Image.asset("assets/images/face.png"))
      ],
    );
  }

  Widget ShowDownStarts(){
    return FutureBuilder(
        future: database.getDataVendor(),
        builder: (_,snapshot){
          if(snapshot.connectionState== ConnectionState.waiting){
            return Container(
                child: Text("Data Loading"));
          }else{
            return DashboardNamePart(snapshot.data['Name']);
          }
        }
    );
  }

  Widget DashboardInfo(){
    return Card(
      child: Column(
        children: <Widget>[
          spacing(16),
          Container(
            width: 320,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: Colors.black45,
                    blurRadius: 5,
                    spreadRadius: 2,
                    offset: Offset(0, 3)
                )
              ],
              gradient: LinearGradient(
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
                colors: [
                  Colors.white,
                  Colors.white
                ],
              ),
              borderRadius: BorderRadius.circular(16),
            ),
            child: Container(
              width: 250,
              child: TabBar(
                controller: Vendor_Controller,
                unselectedLabelColor: Colors.black,
                labelPadding: EdgeInsets.only(bottom: 8,top: 8),
                indicator: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: [
                      Colors.black,
                      Colors.black38,
                    ],
                  ),
                  borderRadius: BorderRadius.circular(16),
                ),
                tabs: <Widget>[
                  Text("This Month"),
                  Text("This Year")
                ],
              ),
            ),
          ),
          spacing(32),
          Container(
            height: 260,
            child: TabBarView(
              controller: Vendor_Controller,
              children: <Widget>[
                VendorDash.ShowDownStartsAgain(),
                Center(child: Text("Year has not yet completed"))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget ShowOrdersVendors(){
    return Column(
      children: <Widget>[
        Row(
            children: <Widget>[
              Expanded(
                child: Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(top: 5),
                    child: Text("ACHIEVEMENT",style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold))),
              )
            ]
        ),
        spacing(16),
        TabBar(
          controller: controller,
          tabs: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 2),
              child: Text("Order Pending",
                  style: TextStyle(fontSize:18, fontWeight: FontWeight.bold,color: Colors.black)),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 2),
              child: Text("Order Completed",
                  style: TextStyle(fontSize:18, fontWeight: FontWeight.bold,color: Colors.black)),
            )
          ],
        ),
        Container(
          height: 450,
          child: TabBarView(
            controller: controller,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Orders.GenerateListOrder_PendingVendor(),
                ],
              ),
              Column(
                children: <Widget>[
                  Orders.GenerateListOrder_CompleteVendor(),
                ],
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget ShowOrdersClients(){
    return Column(
      children: <Widget>[
        Row(
            children: <Widget>[
              Expanded(
                child: Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(top: 5),
                    child: Text("ACHIEVEMENT",style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold))),
              )
            ]
        ),
        spacing(16),
        TabBar(
          controller: controller,
          tabs: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 2),
              child: Text("Order Pending",
                  style: TextStyle(fontSize:18, fontWeight: FontWeight.bold,color: Colors.black)),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 2),
              child: Text("Order Completed",
                  style: TextStyle(fontSize:18, fontWeight: FontWeight.bold,color: Colors.black)),
            )
          ],
        ),
        Container(
          height: 450,
          child: TabBarView(
            controller: controller,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Orders.GenerateListOrder_PendingClient(),
                ],
              ),
              Column(
                children: <Widget>[
                  Orders.GenerateListOrder_CompletedClient(),
                ],
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget ClientDashboard(){
    return Column(
      children: <Widget>[
        Row(
            children: <Widget>[
              Image.asset("assets/images/arrow_back.png", width: 20,
                  height: 20),
              Expanded(
                child: Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(top: 5),
                    child: Text("HOME",style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold))),
              )
            ]
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: Container(
                  alignment: Alignment.center,
                  child: Image.asset("assets/images/objects.png",width: 350,height:250,)),
            )
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(left: 8),
                child: Text("FREELANCERS",style: TextStyle(color: Colors.black,fontSize: 18),))
          ],
        ),
        Row(
          children: <Widget>[
            Builder()
          ],
        ),
      ],
    );
  }



  navigateToPage(DocumentSnapshot post){
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => MyProfile(post: post)));
  }

  Widget Builder(){
    return FutureBuilder(
        future: database.getDataVendorList(),
        builder: (_,snapshot){
          if(snapshot.connectionState== ConnectionState.waiting){
            return Container(
                child: Text("Data Loading"));
          }else{
            return Expanded(
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  itemBuilder: (context,index){
                    return GestureDetector(
                      onTap: () {
                        navigateToPage(snapshot.data[index]);
                        database.getToken();
                      },
                      child: Create_card(snapshot.data[index].data["Name"],
                          snapshot.data[index].data["Email"]),
                    );
                  }
              ),
            );
          }
        }
    );
  }

  Widget Create_card(String name, String desc) {
    return Container(
      margin: EdgeInsets.only(left: 12, right: 12,top: 4),
      //padding: EdgeInsets.only(bottom: 14, top: 14),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.black38,
              blurRadius: 5,
              spreadRadius: 0,
              offset: Offset(0, 3)
          )
        ],
        borderRadius: BorderRadius.circular(10),
      ),
        child: Card(
          //margin: EdgeInsets.only(left: 16,right: 16,top: 8 ,bottom: 4),
          //shape: ShapeBorder.lerp(, 8),
          child: Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 4),
                  child: Image.asset("assets/images/objects.png",width: 65,height:65)),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(name, style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.bold),)
                      ],
                    ),
                    spacing(4),
                    Row(
                      children: <Widget>[
                        Text(desc,style: TextStyle(color: Colors.black,fontSize: 12),)
                      ],
                    )

                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 16),
                  child: Text("150/-",style: TextStyle(fontWeight: FontWeight.bold),))
            ],
          ),

        ),

        );
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }
}