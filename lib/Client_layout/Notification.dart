import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Databse/Databse.dart';

class Notify extends StatefulWidget{
  @override
  NotifyState createState() => NotifyState();
}

class NotifyState extends State<Notify> {
  final database = Database();

  final firestore = Firestore.instance;

  final FirebaseAuth auth = FirebaseAuth.instance;

  String uid;

  @override
  void initState(){
//    firestore.collection("Notification").where("Notification_id",
//        isEqualTo: auth.currentUser()).snapshots();
  database.getCurrentUser().then((value) =>{
    uid = value,
  });

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[

            ],
          ),
        ),
      ),
    );
  }

  Stream<QuerySnapshot> get snap{
    return firestore.collection("Notification").where("Notification_id",
        isEqualTo: uid).snapshots();
  }

  Stream<QuerySnapshot> get snap2{
    return firestore.collection("Notification").where("Vendor_Id",
        isEqualTo: uid).snapshots();
  }


  Widget notify_Vendor(){
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                      alignment: Alignment.center,
                      child: Text("NOTIFICATION",style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold))),
                )
              ]
          ),
            Row(
              children: <Widget>[
                StreamBuilder<QuerySnapshot>(
                  stream: snap2,
                  // ignore: missing_return
                  builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
                    if(!snapshot.hasData){
                      return Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }else{
                      return Expanded(
                        child: ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemCount: snapshot.data.documents.length,
                            itemBuilder: (context,index) {
                              final DocumentSnapshot Ds= snapshot.data.documents[index];
                              return Builder(
                            builder: (context) =>
                             GestureDetector(
                               onTap: (){
                                   Scaffold.of(context).showBottomSheet<void>(
                                 (BuildContext context) {
                                   return Container(
                                   height: 250,
                                    child: bottomSheetVendor(Ds["Notification_title"],
                                        Ds["Notification_Vendor_Id"],
                                        Ds["Notification_Vendor_email"],
                                        Ds["Order_Status"],context,Ds["Get_Known"]),
                                  );
                                },
                                   );
                               },
                              child: Create_card(Ds["Notification_title"]??'default value',
                                  Ds["Notification_Vendor_Id"]??'default value'),
                            ),
                          );
                            }),
                      );
                    }
                  }
                )
              ],
            )
        ],
      ),
    );
  }

  Widget notify_Client(){
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                      alignment: Alignment.center,
                      child: Text("NOTIFICATION",style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold))),
                )
              ]
          ),
          Row(
            children: <Widget>[
              StreamBuilder<QuerySnapshot>(
                  stream: snap,
                  // ignore: missing_return
                  builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
                    if(!snapshot.hasData){
                      return Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }else{
                      return Expanded(
                        child: ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemCount: snapshot.data.documents.length,
                            itemBuilder: (context,index) {
                              final DocumentSnapshot Ds= snapshot.data.documents[index];
                              return Builder(
                                builder: (context) =>
                                    GestureDetector(
                                      onTap: (){
                                        Scaffold.of(context).showBottomSheet<void>(
                                              (BuildContext context) {
                                            return Container(
                                              height: 250,
                                              child: bottomSheet(Ds["Notification_title"],
                                                  Ds["Notification_Vendor_Id"],
                                                  Ds["Notification_Vendor_email"],
                                                  Ds["Order_Status"],
                                                context),
                                            );
                                          },
                                        );
                                      },
                                      child: Create_card(Ds["Notification_title"]??'default value',
                                          Ds["Notification_Vendor_Id"]??'default value'),
                                    ),
                              );
                            }),
                      );
                    }
                  }
              )
            ],
          )
        ],
      ),
    );
  }

  Widget bottomSheet(String Noti_title,String Noti_sender_to, String Noti_mail,String Noti_Status,BuildContext context){
    return Card(
     child: Column(
       children: <Widget>[
         Row(
           mainAxisAlignment: MainAxisAlignment.spaceBetween,
           children: <Widget>[
             Container(
                 margin: EdgeInsets.only(left: 16,top: 8),
                 child: Text("Notification",
                     style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold))),
             GestureDetector(
                 onTap: () => Navigator.pop(context),
                 child: Container(
                     margin: EdgeInsets.only(right: 8),
                     child: Image.asset("assets/images/Close.png")))
           ],
         ),
         spacing(16),
         Row(
           mainAxisAlignment: MainAxisAlignment.center,
           children: <Widget>[
             Text(Noti_title,style: TextStyle(color: Colors.black,
                 fontSize:24,
                 fontWeight: FontWeight.bold))
           ],
         ),
         Row(
           mainAxisAlignment: MainAxisAlignment.center,
           children: <Widget>[
             Text(Noti_sender_to,style: TextStyle(color: Colors.black,
                 fontSize:18,
                 fontWeight: FontWeight.bold))
           ],
         ),
         spacing(8),
         Row(
           mainAxisAlignment: MainAxisAlignment.center,
           children: <Widget>[
             Text(Noti_mail,style: TextStyle(color: Colors.black,
                 fontSize:18,
                 fontWeight: FontWeight.bold))
           ],
         ),
         spacing(8),
         Row(
           mainAxisAlignment: MainAxisAlignment.center,
           children: <Widget>[
             Text(Noti_Status,style: TextStyle(color: Colors.black,
                 fontSize:18,
                 fontWeight: FontWeight.bold))
           ],
         )
       ],
     ),
    );
  }

  Widget bottomSheetVendor(String Noti_title,String Noti_sender_to, String Noti_mail,String Noti_Status,
      BuildContext context,String Id){
    return Card(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(left: 16,top: 8),
                  child: Text("Notification",
                      style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold))),
              GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: Container(
                      margin: EdgeInsets.only(right: 8),
                      child: Image.asset("assets/images/Close.png")))
            ],
          ),
          spacing(16),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(Noti_title,style: TextStyle(color: Colors.black,
                  fontSize:24,
                  fontWeight: FontWeight.bold))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(Noti_sender_to,style: TextStyle(color: Colors.black,
                  fontSize:18,
                  fontWeight: FontWeight.bold))
            ],
          ),
          spacing(8),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(Noti_mail,style: TextStyle(color: Colors.black,
                  fontSize:18,
                  fontWeight: FontWeight.bold))
            ],
          ),
          spacing(8),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(Noti_Status,style: TextStyle(color: Colors.black,
                  fontSize:18,
                  fontWeight: FontWeight.bold))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FlatButton(
                child: Text("Accept"),
                onPressed: () {
                  database.StatusUpdateAccept(Id).then((value) => print("Order Accepted"));
                },
              ),
              FlatButton(
                child: Text("Reject"),
                onPressed: () {
                  database.StatusUpdateRejected(Id).then((value) => print("Order Rejected"));
                },
              )
            ],
          )
        ],
      ),
    );
  }

  Widget Create_card(String name, String desc) {
    return Container(
      margin: EdgeInsets.only(left: 15, right: 15,top: 8,bottom: 8),
      //padding: EdgeInsets.only(bottom: 14, top: 14),
      child: Card(
        //margin: EdgeInsets.only(left: 16,right: 16,top: 8 ,bottom: 4),
        //shape: ShapeBorder.lerp(, 8),
        child: Row(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(right: 4),
                child: Image.asset("assets/images/objects.png",width: 65,height:65)),
            Expanded(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(name, style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.bold),)
                    ],
                  ),
                  spacing(4),
                  Row(
                    children: <Widget>[
                      Text(desc,style: TextStyle(color: Colors.black,fontSize: 12),)
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }
}