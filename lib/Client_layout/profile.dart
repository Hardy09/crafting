import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Client_layout/Book.dart';


//void main() {
//  runApp(MaterialApp(
//    home: MyProfile(),
//  ));
//}

class MyProfile extends StatefulWidget {
  Profile createState() => Profile();
  final DocumentSnapshot post;
  MyProfile(
      {this.post}
      );
}

class Profile extends State<MyProfile> {
  final mainpage= BookMe();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Row(children: <Widget>[
                Image.asset("assets/images/arrow_back.png",
                    width: 20, height: 20),
                Expanded(
                  child: Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: 5),
                      child: Text("PROFILE",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold))),
                )
              ]),
              Row(children: <Widget>[
                Create_card(),
              ]),
              spacing(8),
              //spacing(16),
              Center_Row(),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(bottom: 16),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      log_In_Card("CONTACT"),
                      GestureDetector(
                        onTap: (){
                          navigateToPage(widget.post);
                        },
                          child: log_In_Card("BOOK  ME")),
                      spacing(16)
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future navigateToPage(DocumentSnapshot snapshot) async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => BookMe(post: snapshot,)));
  }
  Widget Center_Row(){
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 16, top: 12),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text("Description Title -",
                      style: TextStyle(fontSize: 22)),
                ],
              ),
              spacing(4),
              Row(
                children: <Widget>[
                  Text(
                    widget.post.data["Desc Title"],
                    style: TextStyle(fontSize: 12,color: Colors.black38),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 16,top: 22),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text("Description -", style: TextStyle(fontSize: 22),),
                ],
              ),
              spacing(4),
              Row(
                children: <Widget>[
                  Expanded(child: Text(widget.post.data['Desc'],style: TextStyle(fontSize: 12,color: Colors.black38),maxLines: 5))
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 16, top: 22),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text("Pricing -",
                      style: TextStyle(fontSize: 22)),
                ],
              ),
              spacing(4),
              Row(
                children: <Widget>[
                  Text(
                    widget.post.data["Money"],
                    style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left:16,top: 22),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text("Why do you hire me?",
                      style: TextStyle(fontSize: 22)
                  ),
                ],
              ),
              spacing(4),
              Row(
                children: <Widget>[
                  Text("Lorem Ipsum is simply dummy text of the printing\n and typesetting industry."
                      "Lorem Ipsum has been the \nindustry's standard dummy text ever since the 1500s,\n"
                      " when an unknown printer took a galley\n of type and scrambled it to make a type \n"
                      "specimen book.",style: TextStyle(color: Colors.black38),),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget Create_card() {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(left: 8, top: 12),
        //padding: EdgeInsets.only(bottom: 14, top: 14),
        child: Row(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(right: 4),
                child: Image.asset("assets/images/objects.png",
                    width: 80, height: 65)),
            Expanded(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        widget.post.data["Name"],
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  spacing(4),
                  Row(
                    children: <Widget>[
                      Text(
                        "Designer",
                        style: TextStyle(color: Colors.black38, fontSize: 12),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }

  Widget log_In_Card(String text) {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16),
      padding: EdgeInsets.only(bottom: 14, top: 14,left: 14,right: 14),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.black45,
              blurRadius: 5,
              spreadRadius: 2,
              offset: Offset(0, 3)
          )
        ],
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: [
            Colors.black,
            Colors.white,
          ],
        ),
        borderRadius: BorderRadius.circular(32),
      ),
      child: Text(
        text,
        style: TextStyle(fontSize: 16, color: Colors.white),
      ),
    );
  }
}
