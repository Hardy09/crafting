import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Databse/Databse.dart';


//void main() {
//  runApp(MaterialApp(
//    home: MainProfile(),
//  ));
//}

class MainProfile extends StatefulWidget {
  myProfile createState() => myProfile();
}

class myProfile extends State<MainProfile>{
  final FirebaseMessaging fcm = FirebaseMessaging();
  final database = Database();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build-
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Builder(),
            ],
          ),
        ),
      ),
    );
  }

  Widget BuilderVendor(){
    return FutureBuilder(
        future: database.getDataVendor(),
        // ignore: missing_return
        builder: (_,snapshot){
          if(snapshot.hasData){
            return MainProf(snapshot.data["Name"],snapshot.data["Email"]);
          }else{
            return Expanded(
              child: Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()),
            );
          }
        }
    );
  }

  Widget BuilderClient(){
    return FutureBuilder(
    future: database.getDataClient(),
    // ignore: missing_return
    builder: (_,snapshot){
      if(snapshot.hasData){
        return MainProf(snapshot.data["Name"],snapshot.data["Email"]);
      }else{
        return Expanded(
          child: Align(
            alignment: Alignment.center,
              child: CircularProgressIndicator()),
        );
      }
    }
  );
  }

  Widget MainProf(String name ,String mail){
    return Column(
      children: <Widget>[
        Container(
          color: Color(0xFFE5E4E4),
          height: 300,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  spacing(48),
                  Image.asset("assets/images/MainProfile_Photo.png"),
                  spacing(16),
                  Row(
                    children: <Widget>[
                      Text(name,style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),)
                    ],
                  ),
                  spacing(16),
                  Row(
                    children: <Widget>[
                      Text("Product Designer")
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(mail)
                    ],
                  )
                ],
              )
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          //crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Container(
              height: 200,
              alignment: Alignment.bottomCenter,
              child: log_Out("LOG OUT"),
            ),
          ],
        )
      ],
    );
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }
  Widget log_Out(String text) {
    return Container(
     // margin: EdgeInsets.only(left: 16, right: 16),
      padding: EdgeInsets.only(bottom: 12, top: 12,left: 16,right: 16),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.black45,
              blurRadius: 5,
              spreadRadius: 2,
              offset: Offset(0, 3)
          )
        ],
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: [
            Colors.black,
            Colors.black38,
          ],
        ),
        borderRadius: BorderRadius.circular(32),
      ),
      child: Text(
        text,
        style: TextStyle(fontSize: 16, color: Colors.white),
      ),
    );
  }

}
