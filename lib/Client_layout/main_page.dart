import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Client_layout/Log_In.dart';
import 'Sign_up.dart';

void main() {
  runApp(MaterialApp(
    home: MainPage(),
  ));
}

class MainPage extends StatefulWidget {
  @override
  MainPageStart createState() => MainPageStart();
}

class MainPageStart extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 64,bottom: 64),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset("assets/images/Crafting.png")
                      ],
                    ),
                    spacing(32),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset("assets/images/Logo.png",
                            width: 150, height: 150)
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 32,bottom: 32),
                child: Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: (){
                        navigateToPageLogIn();
                      },
                        child: card("LOG IN")),
                    spacing(16),
                    Text("Don't have an account!",
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                            fontStyle: FontStyle.italic)),
                    spacing(16),
                    GestureDetector(
                      onTap: () {
                        navigateToPageSignUp();
                      },
                        child: card2("SIGN UP"))
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  navigateToPageSignUp(){
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Sign_up()));
  }

  navigateToPageLogIn(){
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Log_In()));
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }

  Widget padding(double x) {
    return Container(
      padding: EdgeInsets.only(top: x),
    );
  }

  Widget card(String text) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(left: 16, right: 16),
        padding: EdgeInsets.only(bottom: 14, top: 14),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            colors: [
              Colors.black,
              Colors.white,
            ],
          ),
          borderRadius: BorderRadius.circular(32),
        ),
        child: Center(
            child: Text(
          text,
          style: TextStyle(fontSize: 16, color: Colors.white),
        )),
      ),
    );
  }

  Widget card2(String text) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(left: 16, right: 16),
        padding: EdgeInsets.only(bottom: 14, top: 14),
        //color: Color(0xFF2058E8),
        //color: Color(0xFFFDDB89),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.black,
            width: 2,
          ),
          borderRadius: BorderRadius.circular(32),
        ),
        child: Center(child: Text(text, style: TextStyle(fontSize: 16))),
      ),
    );
  }
}
