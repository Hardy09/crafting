import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Databse/Databse.dart';

//void main() {
//  runApp(MaterialApp(
//    home: Order(),
//  ));
//}

class Order extends StatefulWidget{
  @override
  OrderState createState() => OrderState();
}

class OrderState extends State<Order> with SingleTickerProviderStateMixin{
  final database = Database();
  Widget my;
  bool flag = false;
  TabController controller;
  final firestore = Firestore.instance;
  String uid;

  @override
  void initState(){
    super.initState();
    controller = TabController(length: 2,vsync: this);
    database.getCurrentUser().then((value) =>{
      uid = value,
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: 5),
                          child: Text("ACHIEVEMENT",style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold))),
                    )
                  ]
              ),
             // ShowOrders()
            ],
          ),
        ),
      ),
    );
  }

  Widget GenerateListOrder_CompletedClient(){
    return Row(
      children: <Widget>[
        FutureBuilder(
          future: database.getClientsCompletedOrder(),
          builder: (_,snapshot){
            if(snapshot.connectionState == ConnectionState.waiting) {
              return Expanded(
                  child: Container(
                      alignment: Alignment.center,
                      child: CircularProgressIndicator()));
            }else{
              return Expanded(
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: snapshot.data.length,
                    itemBuilder: (context,index){
                      if(snapshot.data.length != 0){
                        return BuildingOrder(snapshot.data[index]
                            .data["Vendor_Name"] ?? 'default value',
                            snapshot.data[index].data["Vendor_email"] ??
                                'default value');
                      }else{
                        return Center(
                          child: Text("No Orders Yet"),
                        );
                      }
                    }
                ),
              );
            }
          },
        )
      ],
    );
  }

  Widget GenerateListOrder_PendingClient(){
    return Row(
      children: <Widget>[
        FutureBuilder(
        future: database.getClientsOrderPending(),
        builder: (_,snapshot){
          if(snapshot.connectionState== ConnectionState.waiting) {
            return Expanded(
                child: Container(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator()));
          }else{
            return Expanded(
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  // ignore: missing_return
                  itemBuilder: (context,index) {
                    if(snapshot.data.length != 0){
                      return BuildingOrder(snapshot.data[index]
                          .data["Vendor_Name"] ?? 'default value',
                          snapshot.data[index].data["Vendor_email"] ??
                              'default value');
                    }else{
                      return Center(
                        child: Text("No Orders Yet"),
                      );
                    }
                  }
              ),
            );
          }
        },
      )
      ],
    );
  }

  Widget GenerateListOrder_CompleteVendor(){
    return Row(
      children: <Widget>[
        StreamBuilder<QuerySnapshot>(
            stream: Ordersnap2,
            // ignore: missing_return
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
              if(!snapshot.hasData){
                return Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(),
                  ),
                );
              }else{
                return Expanded(
                  child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context,index) {
                        final DocumentSnapshot Ds= snapshot.data.documents[index];
                        return Builder(
                          builder: (context) =>
                              GestureDetector(
                                onTap: (){
                                  Scaffold.of(context).showBottomSheet<void>(
                                        (BuildContext context) {
                                      return Container(
                                        height: 250,
                                        child: bottomSheetVendor(Ds["Vendor_email"],Ds["Status"],
                                            context,Ds["Client_User"],Ds["Order_id"]),
                                      );
                                    },
                                  );
                                },
                                child: BuildingOrder(Ds["Vendor_Name"]??'default value',
                                    Ds["Vendor_email"]??'default value'),
                              ),
                        );
                      }),
                );
              }
            }
        )
      ],
    );
  }

  Stream<QuerySnapshot> get Ordersnap{
    return firestore.collection("Orders").where("Vendor_User",
        isEqualTo: uid).where("Status",isEqualTo: "Pending").snapshots();
  }

  Stream<QuerySnapshot> get Ordersnap2{
    return firestore.collection("Orders").where("Vendor_User",
        isEqualTo: uid).where("Status",isEqualTo: "Completed").snapshots();
  }

  Widget GenerateListOrder_PendingVendor(){
  return Row(
    children: <Widget>[
      StreamBuilder<QuerySnapshot>(
          stream: Ordersnap,
          // ignore: missing_return
          builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
            if(!snapshot.hasData){
              return Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(),
                ),
              );
            }else{
              return Expanded(
                child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (context,index) {
                      print(snapshot.data.documents.length);
                      if(snapshot.data.documents.length == 0){
                        return Center(
                          child: Text("No Pending Orders"),
                        );
                      }else{
                        final DocumentSnapshot Ds= snapshot.data.documents[index];
                        return Builder(
                          builder: (context) =>
                              GestureDetector(
                                onTap: (){
                                  Scaffold.of(context).showBottomSheet<void>(
                                        (BuildContext context) {
                                      return Container(
                                        height: 250,
                                        child: bottomSheetVendor(Ds["Vendor_email"],Ds["Status"],
                                            context,Ds["Client_User"],Ds["Order_id"]),
                                      );
                                    },
                                  );
                                },
                                child: BuildingOrder(Ds["Vendor_Name"]??'default value',
                                    Ds["Vendor_email"]??'default value'),
                              ),
                        );
                      }
                    }),
              );
            }
          }
      )
    ],
  );
  }

  Widget bottomSheetVendor(String Client_mail,String Status,
      BuildContext context,String Client_id, String Id){
    return Card(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(left: 16,top: 8),
                  child: Text("Order",
                      style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold))),
              GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: Container(
                      margin: EdgeInsets.only(right: 8),
                      child: Image.asset("assets/images/Close.png")))
            ],
          ),
          spacing(16),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Order Notification",style: TextStyle(color: Colors.black,
                  fontSize:24,
                  fontWeight: FontWeight.bold))
            ],
          ),
          spacing(8),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("From: "+Client_id,style: TextStyle(color: Colors.black,
                  fontSize:14,
                  fontWeight: FontWeight.bold))
            ],
          ),
          spacing(8),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Mail: "+Client_mail,style: TextStyle(color: Colors.black,
                  fontSize:14,
                  fontWeight: FontWeight.bold))
            ],
          ),
          spacing(8),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Current Status: "+Status,style: TextStyle(color: Colors.black,
                  fontSize:14,
                  fontWeight: FontWeight.bold))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                child: Text("Accept",style: TextStyle(color: Colors.black, fontSize:18)),
                onPressed: () {
                  database.OrderStatusUpdateCompleted(Id).then((value) => print("Order Completed"));
                },
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Note: Click accept only when Order is Complete. Changes cannot revert",style: TextStyle(
                fontSize: 8,
              ),maxLines: 2,)
            ],
          )
        ],
      ),
    );
  }



//  Widget bottomSheet(String Noti_title,String Noti_sender_to, String Noti_mail,String Noti_Status,BuildContext context){
//    return Card(
//      child: Column(
//        children: <Widget>[
//          Row(
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[
//              Container(
//                  margin: EdgeInsets.only(left: 16,top: 8),
//                  child: Text("Notification",
//                      style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold))),
//              GestureDetector(
//                  onTap: () => Navigator.pop(context),
//                  child: Container(
//                      margin: EdgeInsets.only(right: 8),
//                      child: Image.asset("assets/images/Close.png")))
//            ],
//          ),
//          spacing(16),
//          Row(
//            mainAxisAlignment: MainAxisAlignment.center,
//            children: <Widget>[
//              Text(Noti_title,style: TextStyle(color: Colors.black,
//                  fontSize:24,
//                  fontWeight: FontWeight.bold))
//            ],
//          ),
//          Row(
//            mainAxisAlignment: MainAxisAlignment.center,
//            children: <Widget>[
//              Text(Noti_sender_to,style: TextStyle(color: Colors.black,
//                  fontSize:18,
//                  fontWeight: FontWeight.bold))
//            ],
//          ),
//          spacing(8),
//          Row(
//            mainAxisAlignment: MainAxisAlignment.center,
//            children: <Widget>[
//              Text(Noti_mail,style: TextStyle(color: Colors.black,
//                  fontSize:18,
//                  fontWeight: FontWeight.bold))
//            ],
//          ),
//          spacing(8),
//          Row(
//            mainAxisAlignment: MainAxisAlignment.center,
//            children: <Widget>[
//              Text(Noti_Status,style: TextStyle(color: Colors.black,
//                  fontSize:18,
//                  fontWeight: FontWeight.bold))
//            ],
//          )
//        ],
//      ),
//    );
//  }

  Widget BuildingOrder(String name, String email){
    return Column(
      children: <Widget>[
        spacing(16),
        Container(
          decoration: BoxDecoration(
              boxShadow: [BoxShadow(
                    color: Colors.black54,
                    blurRadius: 20,
                    spreadRadius: -10
                )]
          ),
          child: Card(
              margin: EdgeInsets.only(top: 8,left: 8,right: 8),
            child: Column(
              children: <Widget>[
                      Create_card(name, email)
                ],
              ),
          ),
        )
      ],
    );
  }

  Widget Create_card(String name, String desc) {
    return Container(
      margin: EdgeInsets.only(left: 15, right: 15,top: 8,bottom: 8),
      //padding: EdgeInsets.only(bottom: 14, top: 14),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.grey,
              blurRadius: 20,
              spreadRadius: -10,
              offset: Offset(0, 0)
          )
        ],
        borderRadius: BorderRadius.circular(10),
      ),
      child: Card(
        //margin: EdgeInsets.only(left: 16,right: 16,top: 8 ,bottom: 4),
        //shape: ShapeBorder.lerp(, 8),
        child: Row(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(right: 4),
                child: Image.asset("assets/images/objects.png",width: 65,height:65)),
            Expanded(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(name, style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.bold),)
                    ],
                  ),
                  spacing(4),
                  Row(
                    children: <Widget>[
                      Text(desc,style: TextStyle(color: Colors.black,fontSize: 12),)
                    ],
                  )
                ],
              ),
            ),
            Container(
                margin: EdgeInsets.only(right: 16),
                child: Text("150/-",style: TextStyle(fontWeight: FontWeight.bold),))
          ],
        ),
      ),
    );
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }
}