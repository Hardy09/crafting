import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Client_layout/Client_front_page.dart';
import 'package:flutter_app/Client_layout/Sign_up.dart';
import 'package:flutter_app/Databse/Databse.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:flutter_app/Databse/Databse.dart';


//void main() {
//  runApp(MaterialApp(
//    home: BookMe(),
//  ));
//}

//void main() => runApp(BookMe());

class BookMe extends StatefulWidget {
  Book createState() => Book();
  final DocumentSnapshot post;
  BookMe(
      {this.post}
      );
}

class Book extends State<BookMe> {
  final database = Database();
  final signIn = Signed_In();
  bool flag = true;
  String name=" ";
  String email=" ";
  @override
  void initState() {
//    database.sendNotification().then((onValue){
//    });
    //selectRadio = 0;
    super.initState();
    addStringToSF();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                  children: <Widget>[
                    Image.asset("assets/images/arrow_back.png", width: 20,
                        height: 20),
                    Expanded(
                      child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: 5),
                          child: Text("BOOK",style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold))),
                    )
                  ]
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Create_card()
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 16, top: 16),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text("Order Details-",
                                  style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold))
                                   ],
                              ),
                          spacing(8),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              RichText(
                                text: TextSpan(
                                    text: "Total- ",
                                    style: TextStyle(color: Colors.black38),
                                    children: <TextSpan>[
                                      TextSpan(text: widget.post.data["Money"],style: TextStyle(color: Colors.black54)),
                                      TextSpan(text: " INR",style: TextStyle(
                                          fontWeight: FontWeight.bold,color: Colors.black))
                                    ]
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              GestureDetector(
                                onTap: (){
                                  database.Orders(widget.post.data["Name"],widget.post.data["Email"]
                                      ,widget.post.data["fcm Token"],widget.post.data["Money"],
                                  widget.post.data["unique Id"],name,email).then((onValue){
                                    print("Orders Data Entered");
                                    print(onValue);
                                  });
                                  database.sendNotification(context,
                                      widget.post.data["Name"],widget.post.data["Email"],
                                      widget.post.data["unique Id"]).then((value) {
                                    print("Notification Send");
                                    alert(widget.post.data["Email"], widget.post.data["Name"]);
                                  });
                                },
                                  child: log_In_Card("DONE"))
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  addStringToSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString("Name");
      email =  prefs.getString("Email");
    });
  }

  Future alert(String email, String name){
   return showDialog(
       context: context,
       builder: (BuildContext context){
         return AlertDialog(
           title: Text("Order Alert"),
           content: Column(
             children: <Widget>[
               Row(
                 children: <Widget>[
                   Expanded(child: Text("Order Notification has sent to-"+ email)),
                 ],
               ),
               spacing(8),
               Row(
                 children: <Widget>[
                   Text("Name- "+ name),
                 ],
               ),
               spacing(8),
               Row(
                 children: <Widget>[
                   Expanded(child: Text("Wait for confirmation from "+
                       name +"\n.Do Check notification/Orders section for updates.")),
                 ],
               )
             ],
           ),
           actions: <Widget>[
             RaisedButton(
               child: Text("OK"),
               onPressed: () {
                 navigateToPage(context,flag);
               },
             )
           ],
         );
       }
   );
  }

  navigateToPage(context, bool flag) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Client_front_page(flag: flag)));
  }
  Widget Create_card() {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(top: 16),
        //padding: EdgeInsets.only(bottom: 14, top: 14),
        child: Row(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(right: 4),
                child: Image.asset("assets/images/objects.png",
                    width: 80, height: 65)),
            Expanded(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        "Description Title",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  spacing(4),
                  Row(
                    children: <Widget>[
                      Text(
                        widget.post.data[ "Desc Title"],
                        style: TextStyle(color: Colors.black38, fontSize: 12),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }


  Widget log_In_Card(String text) {
    return Container(
      width: 150,
      margin: EdgeInsets.only(top: 16),
      padding: EdgeInsets.only(bottom: 14, top: 14,left: 16,right: 16),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.black45,
              blurRadius: 5,
              spreadRadius: 2,
              offset: Offset(0, 3)
          )
        ],
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: [
            Colors.black,
            Colors.white,
          ],
        ),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            text,
            style: TextStyle(fontSize: 16, color: Colors.white),
          ),
        ],
      ),
    );
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }
}