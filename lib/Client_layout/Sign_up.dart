import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Databse/Databse.dart';
import 'package:flutter_app/Client_layout/Client_front_page.dart';
import 'package:flutter_app/Vendor_layout/Details.dart';
import 'package:flutter_app/Vendor_layout/Skill1_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

//void main() {
//  runApp(MaterialApp(
//    home: Sign_up(),
//  ));
//}

class Sign_up extends StatefulWidget{
  Signed_In createState() => Signed_In();
}

class Signed_In extends State {
  final GlobalKey<FormState> _registerFormKey = GlobalKey<FormState>();
  final database = Database();
  final mainpage= Client_front_page();
  SharedPreferences prefs;
  int selectRadio;
  bool flag = false;
  bool Client_flag = true;
  TextEditingController emailController= new TextEditingController();
  TextEditingController passwordController= new TextEditingController() ;
  TextEditingController nameController= new TextEditingController();
  TextEditingController phoneController= new TextEditingController() ;
  TextEditingController UpiIdController =  new TextEditingController();

  @override
  void initState() {
    //selectRadio = 0;
     super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset("assets/images/arrow_back.png", width: 20,
                          height: 20),
                      Expanded(
                        child: Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(top: 5),
                            child: Text("CRAFTING", style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.bold))
                        ),
                      )
                    ],
                  ),
                ],
              ),
             Upper(),
             Form(
               key: _registerFormKey,
                 child: lower())
            ],
          ),
        ),
      ),
    );
  }

  Widget Upper() {
    return Container(
      //height: 8,
      padding: EdgeInsets.only(top: 30,bottom:10),
      child: Column(
        //mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset("assets/images/Logo.png", width: 50, height: 50,),
            ],
          ),
          spacing(16),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("SIGN UP", style: TextStyle(color: Colors.black,
                  fontSize: 32,
                  fontWeight: FontWeight.bold))
            ],
          ),
          spacing(4),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Welcome",
                  style: TextStyle(color: Colors.black.withOpacity(0.8)))
            ],
          )
        ],
      ),
    );
  }

  Widget lower() {
    return Expanded(
      child: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          // verticalDirection: VerticalDirection.down,
          children: <Widget>[
            Row(
              children: <Widget>[
                InputBox("Name",nameController,Namevalidator),
              ],
            ),
            spacing(4),
            Row(
              children: <Widget>[
                InputBox("Phone No",phoneController,Phonevalidator),
              ],
            ),
            spacing(4),
            Row(
              children: <Widget>[
                InputBox("Email",emailController,emailValidator),
              ],
            ),
            spacing(4),
            Row(
              children: <Widget>[
                InputBox("Password",passwordController,PasswordValidator),
              ],
            ),
            spacing(4),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                bottomCard0("VENDOR", 0),
                bottomCard0("CLIENT", 1)
              ],
            ),
            spacing(8),
            if(flag)
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                    InputBox("UPI ID",UpiIdController,Upivalidator)
                ],
            ),
            spacing(8),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: (){
                    if(flag) {
                      if(_registerFormKey.currentState.validate()){
                        database.signUp(
                            emailController.text, passwordController.text).then((onValue){
                          print("Success");
                          database.UserVendor(
                              nameController.text, phoneController.text, UpiIdController.text,
                              emailController.text,passwordController.text).then((onValue){
                                print("I am here");
                            navigateToPageSkill(context);
                          });
                        });
                      }else{
                        print("VENDOR ERROR");
                      }
                    }else{
                      if(_registerFormKey.currentState.validate()){
                        database.signUp(
                            emailController.text, passwordController.text).then((onValue){
                          database.UserClient(
                              nameController.text, phoneController.text,
                              emailController.text,passwordController.text).then((onValue){
                            addStringToSF(nameController.text, emailController.text);
                            navigateToPage(context,Client_flag);
                          });
                          print("Success");
                        });
                      }else{
                        print("CLIENT ERRROR");
                      }
                    }
                  },
                    child:Create_card("CREATE ACCOUNT")
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  addStringToSF(String name, String email) async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString('Name', name);
    prefs.setString('Email', email);
  }

  Future navigateToPageSkill(context) async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Skillset()));
  }

  Future navigateToPage(context, bool check) async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Client_front_page(flag: check)));
  }

//  Widget UpiID(){
//   return Row(
//      mainAxisAlignment: MainAxisAlignment.center,
//      children: <Widget>[
//        InputBox("UPI ID")
//      ],
//    );
//  }

  String emailValidator(String value) {
    try {
      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value)) {
        return 'Email format is invalid';
      } else {
        return null;
      }
    } catch(err){
      print('---------------------------------------------------------');
      print(err);
      print('----------------------------------------------------------');
      return "Validator Exception";
    }
  }

  String PasswordValidator(String value) {
    if (value.length < 6) {
      return 'Password must be longer than 8 characters';
    } else {
      return null;
    }
  }

  String Namevalidator(String value){
    if(value.length < 3 ){
      return "Enter valid  name";
    }
  }

  String Phonevalidator(String value){
    if(value.length < 10 ){
      return "Enter valid  Phone";
    }
  }

  String Upivalidator(String value){
//    if(value.startsWith("[\\w]") ){
//      return "Enter valid Upi";
//    }
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }

  Widget padding(double x) {
    return Container(
      padding: EdgeInsets.only(top: x),
    );
  }

  Widget bottomCard0(String text, int radioVal) {
    return Container(
      margin: EdgeInsets.only(left: 8, right: 8, top: 8),
      padding: EdgeInsets.only(left: 16),
      //color: Color(0xFF2058E8),
      //color: Color(0xFFFDDB89),
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
              color: Colors.black45,
              blurRadius: 5,
              spreadRadius: 1,
              offset: Offset(0, 3)
          )
        ],
      ),
      child: Row(
        children: <Widget>[
          Text(text, style: TextStyle(fontSize: 24, color: Colors.white),),
          Radio(
            value: radioVal,
            groupValue: selectRadio,
            activeColor: Colors.blue,
            onChanged: (val) {
              setSelectedRadio(val);
              if(radioVal==0){
                flag= true;
              }
              else if(radioVal==1){
                flag=false;
              }
              else{
                flag=false;
              }
            },
          )
        ],
      ),
    );
  }

  void setSelectedRadio(int val) {
    setState(() {
      selectRadio = val;
    });
  }

  Widget InputBox(String textile, TextEditingController controller, Function  validator) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(left: 16, right: 16),
        padding: EdgeInsets.only(left: 8),
        //color: Color(0xFF2058E8),
        //color: Color(0xFFFDDB89),
        decoration: BoxDecoration(
          color: Color(0xFFFFFFFF),
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
                color: Colors.black45,
                blurRadius: 5,
                spreadRadius: 2,
                offset: Offset(0, 3)
            )
          ],
        ),
//  collapsed to remove underline and hint text
        child: TextFormField(
          validator: validator,
          controller: controller,
          decoration: InputDecoration.collapsed(
            //contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
            hintText: textile,
          ),
          keyboardType: TextInputType.text,
        ),
      ),
    );
  }

  Widget Create_card(String text) {
    return Container(
      //margin: EdgeInsets.only(left: 16, right: 16),
      padding: EdgeInsets.only(bottom: 14, top: 14,left: 20,right: 20),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.black45,
              blurRadius: 5,
              spreadRadius: 2,
              offset: Offset(0, 3)
          )
        ],
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: [
            Colors.black,
            Colors.black38,
          ],
        ),
        borderRadius: BorderRadius.circular(32),
      ),
      child: Center(
          child: Text(
            text,
            style: TextStyle(fontSize: 16, color: Colors.white),
          )),
    );
  }
}