import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Client_layout/Sign_up.dart';
import 'package:flutter_app/Databse/Databse.dart';
import 'package:flutter_app/Client_layout/Client_front_page.dart';

//void main() {
//  runApp(MaterialApp(
//    home: Log_In(),
//  ));
//}

class Log_In extends StatefulWidget{
  Logged_In createState() => Logged_In();
}

class Logged_In extends State {
  final GlobalKey<FormState> _registerFormKey = GlobalKey<FormState>();
  final database = Database();
  final mainpage= Client_front_page();
  bool Client = true;
  bool flag = false;
  TextEditingController emailController= new TextEditingController();
  TextEditingController passwordController= new TextEditingController() ;
  int selectRadio;
  @ override
  void initState(){
    super.initState();
//    selectRadio=0;
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                   // mainAxisSize: MainAxisSize.min,
                    //mainAxisAlignment: MainAxisAlignment.,
                    children: <Widget>[
                      Image.asset("assets/images/arrow_back.png",width: 20,height: 20 ),
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                            margin: EdgeInsets.only(top: 5),
                            child: Text("CRAFTING",style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold))
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Upper(),
              Form(
                key: _registerFormKey,
                  child: lower())
            ],
          ),
        ),
      ),
    );
  }

  Widget Upper(){
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset("assets/images/Logo.png",width: 50,height: 50,),
            ],
          ),
          spacing(16),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("LOG IN",style: TextStyle(color: Colors.black,fontSize: 32,fontWeight: FontWeight.bold))
            ],
          ),
          spacing(4),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Hi !",style: TextStyle(color: Colors.black.withOpacity(0.8)))
            ],
          )
        ],
      ),
    );
  }

  Widget lower(){
    return Expanded(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
         // verticalDirection: VerticalDirection.down,
          children: <Widget>[
            Row(
              children: <Widget>[
                InputBox("Email",emailController,emailValidator),
              ],
            ),
            spacing(8),
            Row(
              children: <Widget>[
                InputBox("Password",passwordController,PasswordValidator),
              ],
            ),
            spacing(8),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                bottomCard0("VENDOR",0),
                bottomCard0("CLIENT",1)
              ],
            ),
            spacing(8),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: (){
                    if(flag) {
                      if(_registerFormKey.currentState.validate()){
                        database.signIn(
                            emailController.text, passwordController.text).then((onValue){
                              navigateToPage(context, !Client);
                        });
                      }else{
                        print("VENDOR ERRROR");
                      }
                    }else{
                      if(_registerFormKey.currentState.validate()){
                        database.signIn(
                            emailController.text, passwordController.text).then((onValue){
                          navigateToPage(context,Client);
                        });
                      }else{
                        print("CLIENT ERRROR");
                      }
                    }
                  },
                    child: log_In_Card("LOGIN")
                )
              ],
            ),
            spacing(20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Forget Your Password!")
              ],
            )
          ],
        ),
      ),
    );
  }

  Future navigateToPage(context, bool check) async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Client_front_page(flag: check)));
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }

  Widget padding(double x) {
    return Container(
      padding: EdgeInsets.only(top: x),
    );
  }
  Widget bottomCard0(String text, int radioVal) {
    return Container(
      margin: EdgeInsets.only(left: 8, right: 8, top: 8),
      padding: EdgeInsets.only( left: 16),
      //color: Color(0xFF2058E8),
      //color: Color(0xFFFDDB89),
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
              color: Colors.black45,
              blurRadius: 5,
              spreadRadius: 1,
              offset: Offset(0, 3)
          )
        ],
      ),
//                  color:[Colors.blue[800],,
      child: Row(
          //child: Text(text, style: TextStyle(fontSize: 24,color: Colors.white))
        children: <Widget>[
          Text(text, style: TextStyle(fontSize: 24,color: Colors.white),),
          Radio(
            value: radioVal,
            groupValue: selectRadio,
            activeColor: Colors.blue,
            onChanged: (val) {
              setSelectedRadio(val);
              if(radioVal==0){
                flag= true;
              }
              else if(radioVal==1){
                flag=false;
              }
//              else{
//                flag=false;
//              } by default false is taken
            },
          )
        ],
      ),
    );
  }

  void setSelectedRadio(int val){
    setState(() {
      selectRadio=val;
    });
  }
  String emailValidator(String value) {
    try {
      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value)) {
        return 'Email format is invalid';
      } else {
        return null;
      }
    } catch(err){
      print('---------------------------------------------------------');
      print(err);
      print('----------------------------------------------------------');
      return "Validator Exception";
    }
  }

  String PasswordValidator(String value) {
    if (value.length < 6) {
      return 'Password must be longer than 8 characters';
    } else {
      return null;
    }
  }

  Widget InputBox(String textile, TextEditingController controller, Function  validator) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(left: 16, right: 16),
        padding: EdgeInsets.only(left: 8),
        //color: Color(0xFF2058E8),
        //color: Color(0xFFFDDB89),
        decoration: BoxDecoration(
          color: Color(0xFFFFFFFF),
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
                color: Colors.black45,
                blurRadius: 5,
                spreadRadius: 2,
                offset: Offset(0, 3)
            )
          ],
        ),
//  collapsed to remove underline and hint text
        child: TextFormField(
          validator: validator,
          controller: controller,
          decoration: InputDecoration.collapsed(
            //contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
            hintText: textile,
          ),
          keyboardType: TextInputType.text,
        ),
      ),
    );
  }

  Widget log_In_Card(String text) {
    return Container(
      //margin: EdgeInsets.only(left: 16, right: 16),
      padding: EdgeInsets.only(bottom: 14, top: 14,left: 20,right: 20),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.black45,
              blurRadius: 5,
              spreadRadius: 2,
              offset: Offset(0, 3)
          )
        ],
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: [
            Colors.black,
            Colors.white,
          ],
        ),
        borderRadius: BorderRadius.circular(32),
      ),
      child: Center(
          child: Text(
            text,
            style: TextStyle(fontSize: 16, color: Colors.white),
          )),
    );
  }
}