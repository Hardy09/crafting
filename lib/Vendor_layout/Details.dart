//import 'dart:ffi';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Client_layout/Client_front_page.dart';
import 'package:flutter_app/Databse/Databse.dart';

//void main() => runApp(Details());

class Details extends StatefulWidget {
  MyDetails createState() => MyDetails();
}

class MyDetails extends State {
  final GlobalKey<FormState> _details = GlobalKey<FormState>();
  final database = Database();
  TextEditingController descTitle= new TextEditingController();
  TextEditingController desc = new TextEditingController();
  TextEditingController cate = new TextEditingController();
  TextEditingController time = new TextEditingController();
  TextEditingController money = new TextEditingController();
  bool flag = false;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Row(children: <Widget>[
                Image.asset("assets/images/arrow_back.png",
                    width: 20, height: 20),
                Expanded(
                  child: Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: 5),
                      child: Text("DETAILS",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold))),
                  )
                ]
              ),
              spacing(16),
             Form(
               key: _details,
               child: FormChild(),
             )
            ],
          ),
        ),
      ),
    );
  }

  Widget FormChild(){
    return  Expanded(
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
            side: BorderSide(
                color: Colors.black54,
                width: 2
            )
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              center_part("Description title", descTitle,320,50,1),
              spacing(2),
              center_part("Description", desc,320,130,5),
              spacing(2),
              center_part("Category", cate, 155, 50,1),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  center_part("Time(Days)", time, 100, 50,1),
                  Container(
                      margin: EdgeInsets.only(right: 10),
                      child: center_part("Money(Rs)", money, 100, 50,1))
                ],
              ),
              spacing(2),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Bottom_Card("CANCEL"),
                  Container(
                      margin: EdgeInsets.only(left: 16,right: 16),
                      child: GestureDetector(
                          onTap: (){
                            if(_details.currentState.validate()){
                              database.UpdateVendorInfo(descTitle.text, desc.text, cate.text, time.text,
                                  money.text).then((value) => {
                                navigateToPage(context,flag)
                              });
                            }
                          },
                          child: Bottom_Card("ADD"))
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
  Future navigateToPage(context, bool flag) async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Client_front_page(flag: flag)));
  }

  Widget center_part(String text, TextEditingController controller,double wid,double hei, int lines) {
    return Row(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            spacing(16),
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 8),
                  child: Text(
                    text,
                    style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold,fontSize: 18),
                  ),
                ),
              ],
            ),
            spacing(4),
            Container(
              margin: EdgeInsets.only(left: 8),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.black38,
                      blurRadius: 12,
                      spreadRadius: 1,
                      offset: Offset(0, 6)
                  )
                ],
              ),
              child: Card(
                //margin: EdgeInsets.only(left: 8),
                child: Container(
                  width: wid,
                  height: hei,
                  child: TextFormField(
                    validator: (val){
                      if(val.isEmpty){
                        print("Enter Value");
                      }
                      return null;
                    },
                    keyboardType: TextInputType.multiline,
                    maxLines: lines,
                    controller: controller,
                    decoration: InputDecoration.collapsed(
                      hintText: text,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }


  Widget Bottom_Card(String text) {
    return Container(
      margin: EdgeInsets.only(top: 16),
      padding: EdgeInsets.only(bottom: 8, top: 8,left: 20,right: 20),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.black45,
              blurRadius: 5,
              spreadRadius: 0,
              offset: Offset(0, 3)
          )
        ],
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: [
            Colors.black,
            Colors.black12,
          ],
        ),
        borderRadius: BorderRadius.circular(32),
      ),
      child: Text(
        text,
        style: TextStyle(fontSize: 16, color: Colors.white),
      ),
    );
  }
  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }
}
