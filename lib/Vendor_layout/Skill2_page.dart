import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/Databse/Databse.dart';

import 'Details.dart';

//void main() => runApp(Skillset2());

class Skillset2 extends StatefulWidget {
  Skill2 createState() => Skill2();
}

class Skill2 extends State{
  final database = Database();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 16,top: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text("Skip", style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold),)
                  ],
                ),
              ),
              Row(
                children: <Widget>[

                  Image.asset("assets/images/skill2.png")
                ],
              ),
              spacing(16),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Are you a Developer?",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 22),)
                ],
              ),
              spacing(16),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      database.UpdateVendorDeveloperSkill("Yes").then((value) => {
                        navigateToPage(context)
                      });
                    },
                      child: log_In_Card("Yes")),
                  GestureDetector(
                    onTap: (){
                      database.UpdateVendorDeveloperSkill("No").then((value) => {
                        navigateToPage(context)
                      });
                    },
                      child: log_In_Card("No"))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Future navigateToPage(context) async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Details()));
  }

  Widget log_In_Card(String text) {
    return Container(
      margin: EdgeInsets.only(top: 16),
      padding: EdgeInsets.only(bottom: 8, top: 8,left: 20,right: 20),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.black45,
              blurRadius: 5,
              spreadRadius: 2,
              offset: Offset(0, 3)
          )
        ],
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: [
            Colors.black,
            Colors.black12,
          ],
        ),
        //borderRadius: BorderRadius.circular(32),
      ),
      child: Text(
        text,
        style: TextStyle(fontSize: 16, color: Colors.white),
      ),
    );
  }
  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }
}