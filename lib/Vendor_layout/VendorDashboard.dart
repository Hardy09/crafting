import 'package:flutter/material.dart';
import 'package:flutter_app/Databse/Databse.dart';
//
//void main() => runApp(VendorDashBoard());

class VendorDashBoard extends StatefulWidget{
  Dashboard createState() => Dashboard();
}

class Dashboard extends State<VendorDashBoard> {
  final database = Database();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Text("DASHBOARD",style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold)),
              spacing(16),
              Create_card("0  Pending  Orders  !!!"),
              spacing(32),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 16),
                      child: Text("Hello Aniket",style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold))),
                  Container(
                    margin: EdgeInsets.only(right: 16),
                      child: Image.asset("assets/images/face.png"))
                ],
              ),
              spacing(25),
              //finalShowdown()
            ],
          ),
        ),
      ),
    );
  }

  Widget ShowDownStartsAgain(){
    return FutureBuilder(
        future: Future.wait([database.getOrdersDashboard(),database.getOrdersDashboard2()]),
        builder: (_,AsyncSnapshot<List<dynamic>> snapshot){
          if(snapshot.connectionState== ConnectionState.waiting){
            return Container(
                child: Center(child: CircularProgressIndicator()));
          }else{
            return VendorTabBarView1(snapshot.data[0].length.toString() ?? 'No Orders Yet',
                snapshot.data[1].length.toString() ?? 'No Completed Orders');
          }
        }
    );
  }

  Widget getPrice_Vendor_Dashboard(){
    return FutureBuilder(
        future: database.getDataVendor(),
        builder: (_, snapshot){
          if(snapshot.connectionState== ConnectionState.waiting){
            return Container(
                child: Text("Data Loading"));
          }else{
            return Text(snapshot.data['Money'],style: TextStyle(fontWeight: FontWeight.bold));
          }
        }
    );
  }

  Widget VendorTabBarView1(String total_work,String work_accepted){
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 16),
              child: Column(
                children: <Widget>[
                  Text("This Month Earnings",style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
                  spacing(8),
                  Text("120Rs",style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold))
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 16),
              child: Column(
                children: <Widget>[
                  Text("Your Ratings",style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
                  spacing(8),
                  Text("4.5",style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold))
                ],
              ),
            )
          ],
        ),
        spacing(32),
        Text("Job Stats",style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        spacing(32),
        Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 16),
                    child: Text("Jobs Worked")),
                Container(
                    margin: EdgeInsets.only(right: 16),
                    child: Text(total_work,style: TextStyle(fontWeight: FontWeight.bold)))
              ],
            ),
            spacing(16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 16),
                    child: Text("Total No. of Jobs Done")),
                Container(
                    margin: EdgeInsets.only(right: 16),
                    child: Text(work_accepted,style: TextStyle(fontWeight: FontWeight.bold)))
              ],
            ),
            spacing(16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 16),
                    child: Text("Average pay per Work")),
                Container(
                    margin: EdgeInsets.only(right: 16),
                    child: getPrice_Vendor_Dashboard())
              ],
            ),
          ],
        ),
      ],
    );
  }

  Widget OrderPendingCard(){
    return FutureBuilder(
        future: database.getOrdersDashboard3(),
        builder: (_, snapshot){
          if(snapshot.connectionState== ConnectionState.waiting){
            return Container(
                child: Text("Data Loading"));
          }else{
            return Create_card(snapshot.data.length.toString());
          }
        }
    );
  }


  Widget Create_card(String text) {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16),
      padding: EdgeInsets.only(bottom: 14, top: 14,left: 20,right: 20),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.black45,
              blurRadius: 5,
              spreadRadius: 2,
              offset: Offset(0, 3)
          )
        ],
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: [
            Colors.black,
            Colors.black38,
          ],
        ),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Center(
          child: Text(
            text+"  Pending  Orders !!!",
            style: TextStyle(fontSize: 18, color: Colors.white),
          )),
    );
  }
  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }
}