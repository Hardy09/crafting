import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//void main() => runApp(MyApp1());

class MyApp1 extends StatefulWidget {
  @override
  _MyAppState1 createState() => _MyAppState1();
}

class _MyAppState1 extends State<MyApp1> {
  @override
  Widget build(BuildContext context) {
    //Orientation isScreenWide = MediaQuery.of(context).orientation;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              spacing(25),
              card(),
              spacing(60),
              Container(
                child: wrap(6,64,25)
              ),
              spacing(60),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[ singleCard(),
                  singleCard()
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget singleCard(){
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(left: 16,right: 16),
        padding: EdgeInsets.only(top: 8,bottom: 8,left: 8),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            boxShadow: [BoxShadow(color: Colors.black45, blurRadius: 5,spreadRadius: 1, offset: Offset(0,3))],
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors:[Color(0xFF2058E8),Color(0xFF5320E8)],
            )
        ),
        //padding: EdgeInsets.only(top: 32,bottom: 32),
        child: Row(
          children: <Widget>[Image.asset("assets/images/Pay.png",height: 35),padding(45),Text("Pay")],
        ),
      ),
    );
  }
  Widget card(){
    return Container(
      margin: EdgeInsets.only(left: 16,right: 16),
      padding: EdgeInsets.only(top: 32,bottom: 32),
      //color: Color(0xFF2058E8),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          boxShadow: [BoxShadow(color: Colors.black45, blurRadius: 5,spreadRadius: 1, offset: Offset(0,3))],
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors:[Color(0xFF2058E8),Color(0xFF5320E8)],
          )
      ),
//                  color:[Colors.blue[800],,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          cardData("assets/images/Pay.png", "Pay"),
          cardData("assets/images/Pay.png", "Awesome"),
          cardData("assets/images/Pay.png", "Awesome")
        ],
      ),
    );
  }

  Widget wrap(int total, double space,double runspace) {
    return Wrap(
      //alignment: WrapAlignment.center,
      spacing: space,
      runSpacing: runspace,
      // Create a grid with 2 columns. If you change the scrollDirection to
      // horizontal, this produces 2 rows.
      // Generate 100 widgets that display their index in the List.
      children: List.generate(total, (index) {
        return cardData("assets/images/Pay.png", "Awesome");
      }),
    );
  }

  Widget spacing(double x) {
    return Container(
      margin: EdgeInsets.only(top: x),
    );
  }
  Widget padding(double x) {
    return Container(
      padding: EdgeInsets.only(right: x),
    );
  }

  Widget cardData(String Url, String Textpath) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(bottom: 20),
            child: Image.asset(Url, width: 40, height: 40)
        ),
        Text(Textpath,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10))
      ],
    );
  }
}
