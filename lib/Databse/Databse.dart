

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

//void main() {
//  runApp(MaterialApp(
//    home: Database(),
//  ));
//}

// ignore: must_be_immutable
class DatabaseState extends StatefulWidget{

  @override
  Database createState() => Database();
}

class Database extends State<DatabaseState> {
final FirebaseAuth auth = FirebaseAuth.instance;

final FirebaseMessaging fcm = FirebaseMessaging();

var uuid = Uuid();

final databaseReference = Firestore.instance;

SharedPreferences prefs;

List<String> list = List<String>();

TextEditingController emailController = new TextEditingController();

TextEditingController passwordController = new TextEditingController();

TextEditingController nameController = new TextEditingController();

TextEditingController phoneController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Center(
          ),
        ),
      ),
    );
  }

   @override
void initState() {
   // getData();
  //selectRadio = 0;
  //sendNotification();
}

  Future sendNotification(BuildContext context,String vendor_name,String vendor_email, String Vendor_uid) async{
    fcm.configure(
      onMessage:(Map<String,dynamic> message) async{
        print("New message");
        print("onMessage: $message");
        Client_Notification(vendor_name, vendor_email,message['notification']['body'],
            message['data']['message'],Vendor_uid).then((value){
          showDialog(
              context: context,
              builder: (_)=> AlertDialog(
                content: ListTile(
                  title: Text(message['notification']['title']),
                  subtitle: Text(message['notification']['body']),
                ),
                actions: <Widget>[
                  FlatButton(
                    onPressed: ()=> Navigator.of(context).pop(),
                    child: Text("OK"),
                  )
                ],
              )
          );
        });
      },
      onLaunch: (Map<String,dynamic> message) async{
//        print("onMessage: $message");
      },
      onResume: (Map<String,dynamic> message) async{
//        print("onMessage: $message");
      },
    );
  }

  Future<String> Client_Notification(String vendor_name,String vendor_email,String title,
      String message, String Vendor_uid) async{
    String fcmToken = await fcm.getToken();
    final FirebaseUser user = await auth.currentUser();
    final Client_uid = user.uid;
    final same = uuid.v4();
      databaseReference.collection("Notification").document(same).setData({
        "Notification_id": Client_uid,
        "Notification_title": message,
        "Notification_Vendor_Id":vendor_name,
        "Notification_message": title,
        "Notification_Vendor_email" : vendor_email,
        "Order_Status" : "Pending",
        "Get_Known" : same,
        "Vendor_Id" : Vendor_uid
      }).then((onValue){
        print("Notification data successfully Entered");
      }).catchError((onError){
        print(onError);
      });
  }

    Future getClientsNotification() async{
      final FirebaseUser user = await auth.currentUser();
      final uid = user.uid;
      final doc =  databaseReference.collection("Notification").where("Notification_id",
          isEqualTo: uid).getDocuments().then((value) => {
         print(value.documents)
      });
  }

  Future<String> OrderStatusUpdateCompleted(String Id) async{
    getCurrentUser().then((onValue){
      databaseReference.collection("Orders").document(Id).updateData({
        "Status": "Completed"
      }).then((onValue){
        print("Success");
        return onValue;
      });
    });
  }

  Future<String> StatusUpdateAccept(String Id) async{
      getCurrentUser().then((onValue){
        databaseReference.collection("Notification").document(Id).updateData({
          "Order_Status": "Accepted"
        }).then((onValue){
          print("Success");
          return onValue;
        });
      });
  }

  Future<String> StatusUpdateRejected(String Id) async{
    getCurrentUser().then((onValue){
      databaseReference.collection("Notification").document(Id).updateData({
        "Order_Status": "Rejected"
      }).then((onValue){
        print("Success");
        return onValue;
      });
    });
  }

  Future<String> getToken() async{
    String fcmToken = await fcm.getToken();
    if(fcmToken !=null){
      getCurrentUser().then((onValue){
        databaseReference.collection("Clients").document(onValue).updateData({
          "Token": fcmToken
        }).then((onValue){
          print("Success");
          return onValue;
        });
      });
    }
  }

  Future Subscribe() async{
    fcm.subscribeToTopic("flutter app");
  }

  Future<String> signIn(String email, String password) async {
    AuthResult result = await auth.signInWithEmailAndPassword(
        email: email, password: password);
    FirebaseUser user = result.user;
    print(user.uid);
    return user.uid;
  }

  Future<String> getCurrentUser() async {
    FirebaseUser user = await auth.currentUser();
    print(user.uid);
    return user.uid;
  }

  Future<String> signUp(String email, String password) async {
    AuthResult result = await auth.createUserWithEmailAndPassword(
        email: email, password: password);
    FirebaseUser user = result.user;
    return user.uid;
  }

  Future getDataVendorList() async{
    QuerySnapshot qn= await databaseReference.collection("Vendors").getDocuments();
    print(qn.documents.length);
    return qn.documents;
  }

  Future<List<DocumentSnapshot>> getClientsCompletedOrder() async{
    final FirebaseUser user = await auth.currentUser();
    final uid = user.uid;
    QuerySnapshot snapshot = await databaseReference.collection("Orders")
        .where("Client_User", isEqualTo: uid).where("Status", isEqualTo: "Completed").getDocuments();
    print(snapshot.documents.length);
    snapshot.documents.forEach((element) {
      print(element.documentID);
    });
    return snapshot.documents;
  }

  Future<List<DocumentSnapshot>> getClientsOrderPending() async{
    final FirebaseUser user = await auth.currentUser();
    final uid = user.uid;
    QuerySnapshot snapshot = await databaseReference.collection("Orders")
        .where("Client_User", isEqualTo: uid).where("Status", isEqualTo: "Pending").getDocuments();
    print(snapshot.documents.length);
    return snapshot.documents;
  }

  Future<List<DocumentSnapshot>> getVendorCompletedOrder() async{
    final FirebaseUser user = await auth.currentUser();
    final uid = user.uid;
    QuerySnapshot snapshot = await databaseReference.collection("Orders")
        .where("Vendor_User", isEqualTo: uid).where("Status", isEqualTo: "Completed").getDocuments();
    print(snapshot.documents.length);
    snapshot.documents.forEach((element) {
      print(element.documentID);
    });
    return snapshot.documents;
  }

  Future<List<DocumentSnapshot>> getOrdersDashboard() async{
    final FirebaseUser user = await auth.currentUser();
    final uid = user.uid;
      QuerySnapshot snapshot = await databaseReference.collection("Orders").where("Vendor_User",isEqualTo: uid)
    .getDocuments();
      print(snapshot.documents);
      return snapshot.documents;
  }

  Future<List<DocumentSnapshot>> getOrdersDashboard2() async{
    final FirebaseUser user = await auth.currentUser();
    final uid = user.uid;
    QuerySnapshot snapshot = await databaseReference.collection("Orders").where("Vendor_User",isEqualTo: uid)
        .where("Status",isEqualTo: "Completed").getDocuments();
    print(snapshot.documents);
    return snapshot.documents;
  }

  Future<List<DocumentSnapshot>> getOrdersDashboard3() async{
    final FirebaseUser user = await auth.currentUser();
    final uid = user.uid;
    QuerySnapshot snapshot = await databaseReference.collection("Orders").where("Vendor_User",isEqualTo: uid)
        .where("Status",isEqualTo: "Pending").getDocuments();
    print(snapshot.documents);
    return snapshot.documents;
  }

  Future getDataClient() async{
    final FirebaseUser user = await auth.currentUser();
    final uid = user.uid;
   DocumentSnapshot snapshot = await databaseReference.collection("Clients").document(uid).get();
   print(snapshot.data);
    return snapshot.data;
  }

  Future getDataVendor() async{
    final FirebaseUser user = await auth.currentUser();
    final uid = user.uid;
    DocumentSnapshot snapshot = await databaseReference.collection("Vendors").document(uid).get();
    print(snapshot.data);
    return snapshot.data;
  }

  Future getDataOfLoginVendor() async{
    final FirebaseUser user = await auth.currentUser();
    final uid = user.uid;
    DocumentSnapshot snapshot = await databaseReference.collection("Vendors").document(uid).get();
    print(snapshot.data);
    return snapshot.data;
  }

  Future<String> Orders(String vendor_name, String vendor_id, String token,String price, String uid_Vendor,
      String client_name, String client_mail) async{
   // String fcmToken = await fcm.getToken();
    final FirebaseUser user = await auth.currentUser();
    final uid = user.uid;
    final id =uuid.v4();
    databaseReference.collection("Orders").document(id).setData({
      "Client_name": client_name,
      "Client_mail": client_mail,
    "Order_id": id,
    "Order_Token": token,
    "Client_User": uid,
    "Vendor_User": uid_Vendor,
    "Vendor_Name": vendor_name,
    "Vendor_email" : vendor_id,
    "Price" : price,
    "Status" : "Pending"
    }).then((onValue){
    print("Order data successfully Entered");
    }).catchError((onError){
    print(onError);
    });
  }

  Future<String> UserVendor(String name, String phone, String upi,String email,String password) async {
  // await to get the object of current user
  final FirebaseUser user = await auth.currentUser();
  final uid = user.uid;
  final fcmToken = await fcm.getToken();
  print(uid);
  databaseReference.collection("Vendors").document(uid).setData({
    "Name": name,
    "Phone": phone,
    "Upi_Id": upi,
    "Email": email,
    "Password": password,
    "Designer Skill": "Null",
    "Developer Skill" : "Null",
    "Desc Title" : "Null",
    "Desc" : "Null",
    "Category": "Null",
    "Time" : "Null",
    "Money" : "Null",
    "unique Id" : uid,
    "fcm Token" : fcmToken
  }).then((onValue) {
    print("Data Successfully Entered");
  }).catchError((onError) {
    print("Data not Entered");
    print(onError);
  });
  // here you write the codes to input the data into firestore
}

Future<String> UserClient(String name, String phone,String email,String password) async {
  // await to get the object of current user
  final FirebaseUser user = await auth.currentUser();
  final uid = user.uid;
  print(uid);
  databaseReference.collection("Clients").document(uid).setData({
    "Name": name,
    "Phone": phone,
    "Email": email,
    "Password": password,
    "Token": "Null"
  }).then((onValue) {
    print("Data Successfully Entered");
  }).catchError((onError) {
    print(onError);
  });
  // here you write the codes to input the data into firestore
}

  Future<String> UpdateVendorDesignerSkill(String skill) async{
      getCurrentUser().then((onValue){
        databaseReference.collection("Vendors").document(onValue).updateData({
          "Designer Skill": skill,
        }).then((onValue){
          print("Success");
          return onValue;
        });
      });
  }

  Future<String> UpdateVendorDeveloperSkill(String skill) async{
    getCurrentUser().then((onValue){
      databaseReference.collection("Vendors").document(onValue).updateData({
        "Developer Skill": skill,
      }).then((onValue){
        print("Success");
        return onValue;
      });
    });
  }

  Future<String> UpdateVendorInfo(String descTitle,String desc, String cate,String time,String money) async{
    String fcmToken = await fcm.getToken();
    if(fcmToken !=null){
      getCurrentUser().then((onValue){
        databaseReference.collection("Vendors").document(onValue).updateData({
          "Desc Title" : descTitle,
          "Desc" : desc,
          "Category": cate,
          "Time" : time,
          "Money" : money,
        }).then((onValue){
          print("Success");
          return onValue;
        });
      });
    }
  }

  Future<QuerySnapshot> Notify_Status() {
    getCurrentUser().then((value) => {
    databaseReference.collection("Notification").where("Notification_id",isEqualTo: value).snapshots().listen((event) {
      event.documents.forEach((element) {
        print(element);
        return element;
        });
      })
    });
  }

  Stream<QuerySnapshot>  Notify_All(){
//    getCurrentUser().then((value) => {
//      databaseReference.collection("Notification").snapshots().listen((event) {
//        event.documents.forEach((element) {
//          print(event);
//          return event;
//        });
//      })
//    });
//    databaseReference.collection("Notification").where("Notification_id",isEqualTo: auth.currentUser())
  }
}

//  firestoreInstance
//      .collection("users")
//      .where("address.country", isEqualTo: "USA")
//      .snapshots()
//      .listen((result) {
//    result.documents.forEach((result) {
//      print(result.data);
//    });
//  });


//void _onPressed() {
//  firestoreInstance.collection("users").getDocuments().then((querySnapshot) {
//    querySnapshot.documents.forEach((result) {
//      firestoreInstance
//          .collection("users")
//          .document(result.documentID)
//          .collection("pets")
//          .getDocuments()
//          .then((querySnapshot) {
//        querySnapshot.documents.forEach((result) {
//          print(result.data);
//        });
//      });
//    });
//  });
//}

//void _onPressed() {
//  firestoreInstance
//      .collection("users")
//      .snapshots()
//      .listen((result) {
//    result.documentChanges.forEach((res) {
//      if (res.type == DocumentChangeType.added) {
//        print("added");
//        print(res.document.data);
//      } else if (res.type == DocumentChangeType.modified) {
//        print("modified");
//        print(res.document.data);
//      } else if (res.type == DocumentChangeType.removed) {
//        print("removed");
//        print(res.document.data);
//      }
//    });
//  });
//}