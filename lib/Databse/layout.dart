import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//void main() => runApp(DataInput());
//void main() {
//  runApp(MaterialApp(
//    home: DataInput(),
//  ));
//}

class DataInput extends StatefulWidget {
  //final databaseReference = Firestore.instance;
  _DataInputState1 createState() => _DataInputState1();
}

class _DataInputState1 extends State<DataInput> {
  final FirebaseAuth auth = FirebaseAuth.instance;
  final databaseReference = Firestore.instance;
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  bool value= false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    //getData();
    //createRecord();
   // checkUser();
    //checkLogin();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Expanded(child: checkLogin()),
//              RaisedButton(
//                child: Text('Create Record'),
//                onPressed: () {
//                  //createRecord();
//                  // createRecord();
////                  getData();
////                  print("Aniket");
//
////                  MaterialPageRoute(builder: (context) => SecondRoute());
////                  Navigator.push(context, MaterialPageRoute(builder: (context) => SecondRoute()));
//
//                },
//              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget homePage() {
    return Column(
      children: <Widget>[
        TextFormField(
          controller: emailController,
          textAlign: TextAlign.left,
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            border: InputBorder.none,
            labelText: 'PLEASE ENTER YOUR EMAIL',
            hintStyle: TextStyle(color: Colors.grey),
          ),
        ),
        TextField(
          controller: passwordController,
          textAlign: TextAlign.left,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            border: InputBorder.none,
            labelText: 'ENTER YOUR PASSWORD',
            hintStyle: TextStyle(color: Colors.grey),
          ),
        ),
        RaisedButton(
          child: Text("LOGIN"),
          onPressed: () {
            //authenciateUser(emailController.text, passwordController.text);
            print("SignIN result");
            signIn(emailController.text, passwordController.text);
            navigateToDemoPage(context);
//            print(emailController.text);
//            print(passwordController.text);
            //registerUser();
          },
        )
      ],
    );
  }

  Widget checkLogin(){
    print("Value");
    print(value);
   // print(checkUser());
    checkUser();
    if(value==true){
      return demopage();
    }
    else{
      return homePage();
    }
  }

  void checkUser() {
    getCurrentUser().then((onValue){
      print(onValue);
      setState(() {
        value = true;
      });
    }).catchError((onError){
      print(onError);
//      setState(() {
//        value= false;
//      });
    });
//    return value;
//    if ( != Null) {
//      print("ID");
//      //print(getCurrentUser());
//      return demopage();
//    } else {
//      print("ID11111");
//      //print(auth.currentUser());
//      return homePage();
//    }
  }

  Future<String> signIn(String email, String password) async {
    AuthResult result = await auth.signInWithEmailAndPassword(
        email: email, password: password);
    FirebaseUser user = result.user;
    print(user.uid);
    return user.uid;
  }

  Future<String> getCurrentUser() async {
    FirebaseUser user = await auth.currentUser();
    print(user.uid);
    return user.uid;
  }

//  void authenciateUser(String username, String password) async {
//    auth.createUserWithEmailAndPassword(email: username, password: password)
//        .then((onValue) {
//      print("Hello");
//    }).catchError((onError) {
//      print(onError);
//    });
//  }

  Future<String> signUp() async {
    AuthResult result = await auth.createUserWithEmailAndPassword(
        email: emailController.text, password: passwordController.text);
    FirebaseUser user = result.user;
    return user.uid;
  }


//  Future<String> getcurrentUser() async{
//    FirebaseUser user = await auth.currentUser().then((onValue) {
//      print(onValue.uid);
//    }).catchError((onError){
//      print(onError);
//    });
//    //print(user);
//    return user.uid;
//  }

  Widget namePhone() {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            cardname(),
            cardphone(),
            RaisedButton(
              child: Text("Add details"),
              onPressed: () {
                registerUser();
                navigateToDemoPage(context);
              },
            )
          ],

        ),
      ),
    );
  }

  Future navigateToDemoPage(context) async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => demopage()));
  }

  Widget demopage() {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Center(
            child: Text("SUCESSFULLY REGISTERED"),
          ),
        ),
      ),
    );
  }

  void registerUser() async {
    // await to get the object of current user
    final FirebaseUser user = await auth.currentUser();
    final uid = user.uid;
    print(uid);
    databaseReference.collection("Users").document(uid).setData({
      "Name": nameController.text,
      "Phone": phoneController.text
    }).then((onValue) {
      print("Success");
    }).catchError((onError) {
      print(onError);
    });
    // here you write the codes to input the data into firestore
  }

//  void createRecord() async {
//    await databaseReference.collection("books").document()
//        .setData({ "title": "title", "author": "author" })
//        .then((onValue) {
//      print("HelleoEveryone");
//    })
//        .catchError((onError) {
//      print("Aniket"+onError);
//    });
//
//  }
//  void createRecord() async {
//    await databaseReference.collection("books").document("1").setData({
//      'title': 'Mastering Flutter',
//      'description': 'Programming Guide for Dart'
//    });
//  }

//    DocumentReference ref = await databaseReference.collection("books").add({
//      'title': 'Flutter in Action',
//      'description': 'Complete Programming Guide to learn Flutter'
//    });
//    print(ref.documentID);
//  }

//  void getData() {
////    databaseReference
////        .collection("Books").document("h7YVv90LEXWcdfGxbYif")
////        .get()
////        .then((DocumentSnapshot snapShot) {
////          print(snapShot.documentID);
////    });
//    Firestore.instance
//        .collection('books')
//        .document('1')
//        .get()
//        .then((DocumentSnapshot snapshot) {
//      print(snapshot.documentID);
//      print(snapshot.data);
//    });
//  }
  Future navigateToSubPage(context) async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => namePhone()));
  }

  Widget cardname() {
    return Container(
      margin: EdgeInsets.only(bottom: 8, left: 8, right: 8),
      //padding: EdgeInsets.only(bottom: 16,top: 16),
      //color: Color(0xFF2058E8),
      //color: Color(0xFFFDDB89),
      decoration: BoxDecoration(
        color: Color(0xFFFFFFFF),
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
              color: Colors.black45,
              blurRadius: 5,
              spreadRadius: 1,
              offset: Offset(0, 3))
        ],
      ),
//                  color:[Colors.blue[800],,
      child: TextFormField(
        controller: nameController,
        decoration: InputDecoration(labelText: "name"),
        keyboardType: TextInputType.text,
      ),
    );
  }

  Widget cardphone() {
    return Container(
      margin: EdgeInsets.only(bottom: 8, left: 8, right: 8),
      //padding: EdgeInsets.only(bottom: 16,top: 16),
      //color: Color(0xFF2058E8),
      //color: Color(0xFFFDDB89),
      decoration: BoxDecoration(
        color: Color(0xFFFFFFFF),
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
              color: Colors.black45,
              blurRadius: 5,
              spreadRadius: 1,
              offset: Offset(0, 3))
        ],
      ),
//                  color:[Colors.blue[800],,
      child: TextFormField(
        controller: phoneController,
        decoration: InputDecoration(labelText: "Phone"),
        keyboardType: TextInputType.number,
      ),
    );
  }
}
