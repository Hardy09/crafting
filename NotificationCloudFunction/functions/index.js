const functions = require('firebase-functions');
const admin= require ('firebase-admin');

//const db = admin.firestore();
//const fcm = admin.messaging();

admin.initializeApp(
);

var newData;

//  Selecting on create listener i.e when document is created. It takes 2 parameter snapshot and context
exports.messageTrigger= functions.firestore.document("Orders/{OrderId}").onCreate(async(snapshot,context)=>{
    if(snapshot.empty){
        console.log("No Data Is There");
        return
    }
    var tokens = []
    const tok= await admin.firestore().collection("Orders").get();
    for(var token of tok.docs){
        tokens.push(token.data().Order_Token)
    }
// here order token is key on which we want to send notification
    newData = snapshot.data();
    var payload= {
        notification : {title: "Notification", body:"Order Notification Received from"+" "+newData.Client_name+
         ". Go to Orders/Notification page and respond as soon as possible", sound: "default"},
        data: {click_action:"FLUTTER_NOTIFICATION_CLICK", message: "Order Notification",sound: "default",
                    screen: "Notification.dart"},
    };
    try{
        const response= await admin.messaging().sendToDevice(tokens,payload);
        console.log("Notification sent Successfully");
        console.log(tokens);
    }catch(err){
        console.log("Error sending notification");
    }
});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
